{{-- <!DOCTYPE html>
<html lang="en">

<head>

 <title>Computer Store</title>

</head> --}}
        @extends('layouts.app')

        @section('content')
<div class="container">
    <div class="row">
        <div class="col-3">
            {{-- <h1><a href="{{route('products.index')}}" class="main"><strong>products Store</strong></a></h1> --}}
        </div>
        <div class="col-2 offset-7" align="right">
            {{-- <h1><a href="{{route('products.create.page')}}" class="btn btn-primary">เพิ่มหนังสือ</a></h1> --}}
        </div>
    </div>
    <br><br>
    <div class="row">
        <div class="col-12">
            <table class="table table-hover table-dark">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Product</th>
                    <th scope="col">Image</th>
                    <th scope="col">Amount</th>
                    <th scope="col">Price</th>
                </tr>
                </thead>
                <tbody>
                @foreach($orders as $key => $order)
                    <tr>
                        <th scope="row">{{$key + 1}}</th>
                        <td>{{$order->getRelations()['product']->name}}</td>
                        <td><img style="width:100px" src="{{$order->getRelations()['product']->image}}"></td>
                        <td>{{$order->amount}}</td>
                        <td>{{$order->amount * $order->getRelations()['product']->price}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>
@endsection