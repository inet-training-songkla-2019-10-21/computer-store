<?php
Route::get('/product/update/{id}','UpdateProductController@updateProductPage')->name('product.update.page');
Route::post('/product/update','UpdateProductController@updateProduct')->name('product.update');
Route::get('/admin', 'InsertProductController@IndexPage')->name('admin.index');
Route::get('/insert', 'InsertProductController@CreatePage')->name('admin.create.page');
Route::post('/insert', 'InsertProductController@create')->name('admin.create');
Route::get('/product/delete/{id}','DeleteProductController@deleteProduct')->name('product.delete');
// Route::get('/admin/register', 'AdminLoginController@register')->name('register');
Route::get('/admin/login', 'AdminLoginController@loginPage')->name('admin.login.page');
Route::post('/admin/login', 'AdminLoginController@login')->name('admin.login');
    Route::get('/adminorder', 'AdminOrderController@showOrderAdmin')->name('admin.order.page');
Route::get('/admin/logout', 'AdminLoginController@logout')->name('admin.logout');