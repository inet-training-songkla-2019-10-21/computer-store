{{-- <!DOCTYPE html>
<html lang="en">

<head>

 <title>Computer Store</title>

</head> --}}
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header1">Your order</div>

                
                <div class="card-body">
                <form action="{{route('buy.index')}}" method="post">
                        @csrf
                        <div class="form-group">
                          <div class="form-row">
                            <div class="col-md-3">
                              <div class="form-label-group">
                                <label for="image">Image(url)</label>
                              </div>
                            </div>
                            <div class="col-md-9">
                              <div class="form-label-group">
                                  <td><img class="img-product" style="width:50%" src="{{$products->image}}"></td>
                                    {{-- {{$products->image}} --}}
                                </div>
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                            <div class="form-row">
                              <div class="col-md-3">
                                <div class="form-label-group">
                                  <label for="name">Name</label>
                                </div>
                              </div>
                              <div class="col-md-9">
                                <div class="form-label-group">
                                        {{$products->name }}                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="form-group">
                              <div class="form-row">
                                <div class="col-md-3">
                                  <div class="form-label-group">
                                    <label for="brand">Brand</label>
                                  </div>
                                </div>
                                <div class="col-md-9">
                                  <div class="form-label-group">
                                        {{$products->brand }}
   
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="form-group">
                                <div class="form-row">
                                  <div class="col-md-3">
                                    <div class="form-label-group">
                                      <label for="details">Details</label>
                                    </div>
                                  </div>
                                  <div class="col-md-9">
                                    <div class="form-label-group">
                                        {{$products->describe }}
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="form-group">
                                  <div class="form-row">
                                    <div class="col-md-3">
                                      <div class="form-label-group">
                                        <label for="price">Price</label>
                                      </div>
                                    </div>
                                    <div class="col-md-9">
                                      <div class="form-label-group">
                                            {{$products->price }}
                                      </div>
                                    </div>
                                  </div>
                                </div> 
                                <div class="form-group">
                                    <div class="form-row">
                                      <div class="col-md-3">
                                        <div class="form-label-group">
                                          <label for="amount">Amount</label>
                                        </div>
                                      </div>
                                      <div class="col-md-9">
                                        <div class="form-label-group">
                                            <input type="number" class="form-control" name="amount"/>
                                        <input type="hidden" name="id" value="{{$products->id}}"/>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                        <br>


                        
                        <div align="right">
                            <button type="submit"  class="btn btn-success" onclick="alert('Your order is successful!')">Order</button></form>





                            <a href="{{route('userproduct.index')}}" class="btn btn-cancel">Cancel</a>


                        </div>
                         <!-- Logout Modal-->
                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection