<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;


class AdminLoginController extends Controller
{
    public function register()
    {
        $admin['name'] = 'admin';
        $admin['email'] = 'admin@gmail.com';
        $admin['type'] = 'admin';
        $admin['password'] = '12345678';
        $admin['password']=Hash::make($admin['password']);
        User::create($admin);
        return redirect()->route('admin.index');
    }

    public function loginPage()
    {
        return view('admin.login');
    }
    public function login(Request $request)
    {
        $isAuth = Auth::attempt([
            'email' => $request->email, 
            'password' => $request->password,
            'type' => 'admin'
            ]);
        if(!$isAuth){
            return redirect()->back();
        }
        return redirect()->route('admin.index');
    }
    public function logout()
    {
        Auth::logout();
        return redirect()->route('admin.index');
    }
}
