<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Order;

class Product extends Model
{
    protected $fillable =[
        'image',
        'name',
        'brand',
        'price',
        'describe',
        'amount',
    ];

    public function order()
    {
        return $this->hasMany(Order::class,'id');
    }
}
