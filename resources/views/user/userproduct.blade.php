{{-- <!DOCTYPE html>
<html lang="en">

<head>

 <title>Computer Store</title>

</head> --}}
        @extends('layouts.app')


        @section('content')
        <style type="text/css">
            img.resize  {
                width: 100px;
                height: 100px;
                border: 0;
            }
            img:hover.resize  {
                width: 128px;
                height: 128px;
                border: 0;
            }
            </style>
<div class="container">
    <div class="row">
        <div class="col-3">
            {{-- <h1><a href="{{route('products.index')}}" class="main"><strong>products Store</strong></a></h1> --}}
        </div>
        <div class="col-2 offset-7" align="right">
            {{-- <h1><a href="{{route('products.create.page')}}" class="btn btn-primary">เพิ่มหนังสือ</a></h1> --}}
        </div>
    </div>
    <br><br>
    <div class="row">
        <div class="col-12">
            <table class="table table-hover table-dark">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Image</th>
                    <th scope="col">Describe</th>
                    <th scope="col">Brand</th>
                    <th scope="col">Price</th>
                    <th scope="col">Amount</th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
                @foreach($products as $key => $products)
                    <tr>
                        <th scope="row">{{$key + 1}}</th>
                        <td>{{$products->name}}</td>
                        <td><img class="resize" src="{{$products->image}}" alt="{{$products->image}}"></td>
                        <td>{{$products->describe}}</td>
                        <td>{{$products->brand}}</td>
                        <td>{{$products->price}}</td>
                        <td>{{$products->amount}}</td>
                        {{-- //#ปุ่ม buy ให้ไปเร้าไหนใส่ที่ href ครับ --}}
                        <td><a href="{{route('buy', $products->id)}}" class="btn btn-outline-warning">BUY</a></td>
                        {{-- <td><a href="{{route('products.delete', $products->id)}}" class="btn badge-danger">ลบ</a></td> --}}
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>
@endsection