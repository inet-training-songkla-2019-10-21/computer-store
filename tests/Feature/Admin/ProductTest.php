<?php

namespace Tests\Feature\Admin;


use App\Product;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ProductTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */

     public function testGuestUpdateProduct()
     {
        //old data
        $product = factory(Product::class)->create();
        //new data
        $newProduct = factory(Product::class)->make();

        $this->get(route('product.update.page',$product->id))
        ->assertViewIs('admin.update_product')
        ->assertStatus(200);

        $this->post(route('product.update'), [
            '_token' => \Session::token(),
            'id' => $product->id,
            'name' => $newProduct->name,
            'brand' => $newProduct->brand,
            'price' => $newProduct->price,
            'describe' => $newProduct->describe,
            'amount' => $newProduct->amount,
            'image' => $newProduct->image,
        ])->assertRedirect(route('book.index'));

        $productEdited = Book::find($product->id);
        $this->assertEquals($newProduct->name, $productEdited->name);
        $this->assertEquals($newProduct->brand, $productEdited->author);
        $this->assertEquals($newProduct->price, $productEdited->price);
        $this->assertEquals($newProduct->describe, $productEdited->describe);
        $this->assertEquals($newProduct->amount, $productEdited->amount);
        $this->assertEquals($newProduct->image, $productEdited->image);
     }
}
