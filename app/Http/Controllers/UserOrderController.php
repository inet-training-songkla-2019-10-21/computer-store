<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use Illuminate\Support\Facades\Auth;


class UserOrderController extends Controller
{
    public function showOrder()
    {
        $orders = Order::with('user','product')->where('user',Auth::user()->id)->get();
        return view('user.order', compact('orders'));
    }
}
