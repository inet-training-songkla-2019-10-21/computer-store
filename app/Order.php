<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Product;

class Order extends Model
{
       //Order
        protected $fillable = [
            'user', 
            'product',
            'date',
            'amount',
            'price'
        ];

        public function user()
        {
            return $this->belongsTo(User::class,'user','id');
        }

        public function product()
        {
            return $this->belongsTo(Product::class,'product','id');
        }



}
