<?php

namespace App\Http\Controllers\User;

use App\User;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    public function showRegistrationForm()
    {
        return view('auth.register');
    }
    public function register(Request $request)
    {
        
      $validatedData = $request->validate([
          'name'=> 'required|max:255',
          'email'=>'required|email|max:255',
          'password'=>'required|min:6|confirmed',
      ]);
      
     if(User::where('email',$request->email)->exists()){
         return redirect()->back();
     }
     $validatedData['password']=Hash::make($validatedData['password']);
     User::create($validatedData);
     return redirect()->route('login');
    }
    
}
