<?php
Route::get('register', 'User\RegisterController@showRegistrationForm')->name('register.page');
Route::post('register', 'User\RegisterController@register')->name('register');

Route::get('login', 'User\LoginController@showLoginForm')->name('login');
Route::post('login', 'User\LoginController@login');
Route::post('logout', 'User\LoginController@logout')->name('logout');

Route::get('/login/facebook','User\LoginController@redirectToProvider')->name('login.facebook');
Route::get('/login/facebook/callback','User\LoginController@handleProviderCallBack');
Route::get('/', 'UserProductController@index')->name('userproduct.index');

Route::group(['middleware' => ['user']], function () {
    Route::get('/buy/{id}','BuyProductController@buyPage')->name('buy');
    Route::post('/buy','BuyProductController@buy')->name('buy.index');
    Route::get('/order','UserOrderController@showOrder')->name('order');
});

