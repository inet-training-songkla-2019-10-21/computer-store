<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Computer Store</title>

  <!-- Custom fonts for this template-->
  <link href="/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

  <!-- Page level plugin CSS-->
  <link href="/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="/css/sb-admin.css" rel="stylesheet">

</head>

<body id="page-top">

  <nav class="navbar navbar-expand navbar-dark bg-dark static-top">

  <a class="navbar-brand mr-1" href="{{ route('admin.index') }}">
      <img class="logo-main" src="{{ asset('asset/img/logo-main.jpg') }}">
    </a>

    <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
      <i class="fas fa-bars"></i>
    </button>
  <div class="input-group">
        <div class="input-group-append">
        </div>
      </div>
    <!-- Navbar -->
    <ul class="navbar-nav ml-auto ml-md-0">
      <li class="nav-item dropdown no-arrow">
        <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-user-circle fa-fw"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
          <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">Logout</a>
        </div>
      </li>
    </ul>

  </nav>

  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="sidebar navbar-nav">
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-fw fa-laptop"></i>
          <span>Product</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
          <a class="dropdown-item" href="{{route('admin.index')}}">All Product</a>
          <a class="dropdown-item" href="{{route('admin.create.page')}}">Insert Product</a>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="">
          <i class="fas fa-fw fa-list"></i>
          <span>Order</span>
        </a>
      </li>
    </ul>

    <div id="content-wrapper">

      <div class="container-fluid">

        <!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">Update Product</div>
          <div class="card-body">
                    <form action="{{route("product.update")}}" method="post">
                        @csrf
                        <div class="form-group">
                          <div class="form-row">
                            <div class="col-md-3">
                              <div class="form-label-group">
                                <label for="image">Image(url)</label>
                              </div>
                            </div>
                            <div class="col-md-9">
                              <div class="form-label-group">
                                <input type="text" id="image" name="image" class="form-control" value="{{$product->image}}" required="required">
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                            <div class="form-row">
                              <div class="col-md-3">
                                <div class="form-label-group">
                                  <label for="name">Name</label>
                                </div>
                              </div>
                              <div class="col-md-9">
                                <div class="form-label-group">
                                  <input type="text" id="name" name="name" class="form-control" value="{{$product->name}}" required="required">
                                </div>
                              </div>
                            </div>
                        </div>
                         <div class="form-group">
                              <div class="form-row">
                                <div class="col-md-3">
                                  <div class="form-label-group">
                                    <label for="brand">Brand</label>
                                  </div>
                                </div>
                                <div class="col-md-9">
                                  <div class="form-label-group">
                                      <select class="form-control" id="brand" name="brand">
                                          <option value="Dell" <?=$product->brand == 'Dell' ? ' selected="selected"' : '';?>>Dell</option>
                                          <option value="Asus" <?=$product->brand == 'Asus' ? ' selected="selected"' : '';?>>Asus</option>
                                          <option value="Acer" <?=$product->brand == 'Acer' ? ' selected="selected"' : '';?>>Acer</option>
                                          <option value="Msi" <?=$product->brand == 'Msi' ? ' selected="selected"' : '';?>>Msi</option>
                                          <option value="lenovo" <?=$product->brand == 'lenovo' ? ' selected="selected"' : '';?>>lenovo</option>
                                          <option value="Apple" <?=$product->brand == 'Apple' ? ' selected="selected"' : '';?>>Apple</option>
                                      </select>
                                  </div>
                                </div>
                              </div>
                          </div>
                          <div class="form-group">
                                  <div class="form-row">
                                    <div class="col-md-3">
                                      <div class="form-label-group">
                                        <label for="price">Price</label>
                                      </div>
                                    </div>
                                    <div class="col-md-9">
                                      <div class="form-label-group">
                                        <input type="number" id="price" name="price" class="form-control" value="{{$product->price}}"  required="required">
                                      </div>
                                    </div>
                                  </div>
                          </div> 
                          <div class="form-group">
                                    <div class="form-row">
                                      <div class="col-md-3">
                                        <div class="form-label-group">
                                          <label for="amount">Amount</label>
                                        </div>
                                      </div>
                                      <div class="col-md-9">
                                        <div class="form-label-group">
                                          <input type="number" id="amount" name="amount" class="form-control" value="{{$product->amount}}" required="required">
                                        </div>
                                      </div>
                                    </div>
                          </div>
                          
                          <div class="form-group">
                              <div class="form-row">
                                <div class="col-md-3">
                                  <div class="form-label-group">
                                    <label for="details">Details</label>
                                  </div>
                                </div>
                                <div class="col-md-9">
                                  <div class="form-label-group">
                                    <textarea id="details" name="describe" class="form-control" required="required">{{$product->describe}}</textarea>
                                  </div>
                                </div>
                              </div>
                        </div>
                                      <input type="hidden" name="id" value="{{$product->id}}">
                        <br>
                        <div class="form-group">
                            <div class="form-row">
                              <div class="col-md-3">
                                <div class="form-label-group">
                                </div>
                              </div>
                              <div class="col-md-9">
                                  <button type="submit" name="upload" class="btn btn-success" onclick="alert('Insert Sccessful')">submit</button>
                                  <button type="reset" class="btn btn-secondary" >cancel</button>
                              </div>
                            </div>
                          </div>
                    </form>
                </div>
         
        </div>

      </div>
    </div>
    <!-- /.content-wrapper -->

  </div>
  <!-- /#wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Are you sure? Logout?</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="{{route('admin.login')}}">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="/vendor/jquery/jquery.min.js"></script>
  <script src="/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Page level plugin JavaScript-->
  <script src="/vendor/datatables/jquery.dataTables.js"></script>
  <script src="/vendor/datatables/dataTables.bootstrap4.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="/js/sb-admin.min.js"></script>

</body>

</html>
