<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class UserProductController extends Controller
{
    public function index()
    {
        // dd(1);
        $products = Product::all();
        return view('user.userproduct', compact('products'));
    }
}
