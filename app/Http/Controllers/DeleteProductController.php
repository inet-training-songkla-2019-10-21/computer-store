<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class DeleteProductController extends Controller
{
    public function deleteProduct(Product $id)
    {
        $id->delete();
        return redirect()->route('admin.index');
    }
}
