<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;

class AdminOrderController extends Controller
{
    public function showOrderAdmin()
    {
        $orders = Order::with('user','product')->get();
        return view('admin.adminorder', compact('orders'));
    }
}
