<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class UpdateProductController extends Controller
{
    public function updateProductPage($id)
    {
        $product = Product::find($id);
        return view('admin.update', ['product' => $product]);
    }

    public function updateProduct(Request $request)
    {
        $product = Product::find($request->id);
        $product->image = $request->image;
        $product->name = $request->name;
        $product->brand = $request->brand;
        $product->price = $request->price;
        $product->describe = $request->describe;
        $product->amount = $request->amount;

        if(!$product->save()){
            return redirect()->route('product.update.page');
        }
        return redirect()->route('admin.index');
    }
}
