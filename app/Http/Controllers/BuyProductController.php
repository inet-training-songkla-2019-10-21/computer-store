<?php

namespace App\Http\Controllers;
use App\Product;
use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BuyProductController extends Controller
{

 public function index(){
$products = Product::all();

return view('product.index',compact('products'));
 }

    public function buyPage($id){
         $product = Product::find($id);
        return view('user.buy',['products'=>$product]);
    }

    public function buy(Request $request){
        $orders = new Order();      
        $orders->user = Auth::user()->id;
        $orders->product = $request->id;
        $orders->amount = $request->amount;

        $product = Product::find($request->id);
        $product->amount=$product->amount-$request->amount;
        $product->save();
        $orders->save();
        
    if(!$orders->save()){
    
    return redirect()->route('buy');
    }
    return redirect()->route('userproduct.index');
   }
}