<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
class InsertProductController extends Controller
{

    public function IndexPage(){
        $products = Product::all();
// dd($products);
        return view('admin.index',compact('products'));
    }

    
    public function CreatePage(){
        // $products = Product::all();
// dd($products);
        return view('admin.create');
    }
   
   
    public function create(Request $request)
    {       
        
        // return view('admin.create');
        $product = new Product();        
      
        $product->name = $request->name;
        $product->image = $request->image;
        $product->brand = $request->brand;
        $product->price = $request->price;
        $product->describe = $request->describe;
        $product->amount = $request->amount;

        // dd($book->save());

        if(!$product->save()){
            return redirect()->route('admin.create.page');
        }
        // dd('Insert Product Sccessful');

        return redirect()->route('admin.index');
             
    }
}
